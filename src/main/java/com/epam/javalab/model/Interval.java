package com.epam.javalab.model;

public class Interval {
    private Integer from;
    private Integer to;

    public Interval(int from, int to) {
        this.from = from;
        this.to = to;
    }

    public Integer getFrom() {
        return from;
    }

    public Integer getTo() {
        return to;
    }
}
