package com.epam.javalab.service;


import com.epam.javalab.exception.NoEvenNumberFibonacciException;
import com.epam.javalab.exception.NoOddNumberFibonacciException;

import java.util.ArrayList;
import java.util.List;

public class FibonacciNumberService {

    public void printMaxEvenAndOddNumber(List<Integer> list) {
        System.out.println("Max even number - " + findMaxEvenNumber(list));
        System.out.println("Max odd number - " + findMaxOddNumber(list));
    }

    public double calculatePercentageOfOddNumbers(List<Integer> numbers) {
        int counter = 0;
        for (Integer number : numbers) {
            if (isNumberOdd(number)) {
                counter++;
            }
        }

        return (double) counter / numbers.size() * 100;
    }

    public double calculatePercentageOfEvenNumbers(List<Integer> numbers) {
        int counter = 0;
        for (Integer number : numbers) {
            if (isNumberEven(number)) {
                counter++;
            }
        }

        return (double) counter / numbers.size() * 100;
    }

    public List<Integer> buildFibonacciSequence(int size) {
        List<Integer> fibonacciSequence = new ArrayList<>();
        int firstNumber = 1;
        int secondNumber = 1;

        fibonacciSequence.add(firstNumber);
        fibonacciSequence.add(secondNumber);
        while (fibonacciSequence.size() < size) {
            int temp = firstNumber + secondNumber;
            firstNumber = secondNumber;
            secondNumber = temp;

            fibonacciSequence.add(temp);
        }

        return fibonacciSequence;
    }

    private Integer findMaxEvenNumber(List<Integer> list) {
        for (int i = list.size() - 1; i > 0; i--) {
            if (isNumberEven(list.get(i))) {
                return list.get(i);
            }
        }

        throw new NoEvenNumberFibonacciException();
    }

    private Integer findMaxOddNumber(List<Integer> list) {
        for (int i = list.size() - 1; i > 0; i--) {
            if (isNumberOdd(list.get(i))) {
                return list.get(i);
            }
        }

        throw new NoOddNumberFibonacciException();
    }


    private boolean isNumberOdd(int number) {
        return number % 2 != 0;
    }

    private boolean isNumberEven(int number) {
        return number % 2 == 0;
    }

}
