package com.epam.javalab.service;

import com.epam.javalab.model.Interval;


public class NumberPrintingService {

    public void printOddFromStart(Interval interval) {
        for (int i = interval.getFrom(); i <= interval.getTo(); i++) {
            if (isNumberOdd(i)) {
                System.out.print(i + " ");
            }
        }
    }


    public void printEvenFromEnd(Interval interval) {
        for (int i = interval.getTo(); i >= interval.getFrom(); i--) {
            if (isNumberEven(i)) {
                System.out.print(i + " ");
            }
        }
    }

    public void printSum(Interval interval) {
        int sum = 0;
        for (int i = interval.getFrom(); i <= interval.getTo(); i++) {
            sum += i;
        }
        System.out.println(sum);
    }

    private boolean isNumberOdd(int number) {
        return number % 2 != 0;
    }

    private boolean isNumberEven(int number) {
        return number % 2 == 0;
    }

}
