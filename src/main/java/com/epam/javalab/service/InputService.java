package com.epam.javalab.service;


import com.epam.javalab.model.Interval;
import com.epam.javalab.validator.InputIntervalValidator;

import java.util.Scanner;

public class InputService {

    public Interval enterInterval() {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter first number:");
            String firstNumber = scanner.nextLine();
            System.out.println("Enter second number");
            String secondNumber = scanner.nextLine();

            if (InputIntervalValidator.validate(firstNumber)
                    && InputIntervalValidator.validate(secondNumber)) {
                return new Interval(Integer.parseInt(firstNumber), Integer.parseInt(secondNumber));
            } else{
                System.out.println("Failed");
            }
        }
    }

    public int enterSize() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter size:");
        return scanner.nextInt();
    }

}
