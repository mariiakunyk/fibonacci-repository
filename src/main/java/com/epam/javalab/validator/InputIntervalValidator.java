package com.epam.javalab.validator;

public class InputIntervalValidator {
    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        } catch(NullPointerException e) {
            return false;
        }
        return true;
    }

    public static boolean validate(String number) {
        String[] input = number.split(" ");
        if (input.length >= 2 && isInteger(number)) {
            return false;
        }
        return true;
    }
}
