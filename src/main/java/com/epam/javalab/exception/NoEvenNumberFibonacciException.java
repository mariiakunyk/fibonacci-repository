package com.epam.javalab.exception;

public class NoEvenNumberFibonacciException extends RuntimeException {
    public NoEvenNumberFibonacciException() {
    }

    public NoEvenNumberFibonacciException(String message) {
        super(message);
    }
}
