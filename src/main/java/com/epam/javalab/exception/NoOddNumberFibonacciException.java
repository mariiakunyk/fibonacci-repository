package com.epam.javalab.exception;

public class NoOddNumberFibonacciException extends RuntimeException {
    public NoOddNumberFibonacciException() {
    }

    public NoOddNumberFibonacciException(String message) {
        super(message);
    }
}
