package com.epam.javalab;

import com.epam.javalab.model.Interval;
import com.epam.javalab.service.FibonacciNumberService;
import com.epam.javalab.service.InputService;
import com.epam.javalab.service.NumberPrintingService;

import java.util.List;

public class Application {
    public static void main(String[] args) {
        InputService inputService = new InputService();

        Interval interval = inputService.enterInterval();
        NumberPrintingService printingService = new NumberPrintingService();
        System.out.print("Print even from end and odd from start: ");
        printingService.printEvenFromEnd(interval);
        printingService.printOddFromStart(interval);
        System.out.println();
        System.out.print("Print sum: ");
        printingService.printSum(interval);

        FibonacciNumberService fibonacciNumberService = new FibonacciNumberService();
        int size = inputService.enterSize();

        List<Integer> fibonacciSequence = fibonacciNumberService.buildFibonacciSequence(size);
        System.out.print("Print max even and odd number:");
        System.out.println();
        fibonacciNumberService.printMaxEvenAndOddNumber(fibonacciSequence);
        System.out.println("Print percentage of even numbers:");
        System.out.println(fibonacciNumberService.calculatePercentageOfEvenNumbers(fibonacciSequence));
        System.out.println("Print percentage of odd numbers:");
        System.out.println(fibonacciNumberService.calculatePercentageOfOddNumbers(fibonacciSequence));
    }
}
